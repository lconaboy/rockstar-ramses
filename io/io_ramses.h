#ifndef _IO_RAMSES_H_
#define _IO_RAMSES_H_

#include <stdint.h>
#include "../particle.h"

#if defined(__cplusplus)
extern "C" {
#endif

void load_particles_ramses( const char *filename, struct particle **p, int64_t *num_p );

#if defined(__cplusplus)
}
#endif

#endif /* _IO_GADGET_H_ */
