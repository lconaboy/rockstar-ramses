#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>

extern "C"{
#include "io_util.h"
#include "../universal_constants.h"
#include "../check_syscalls.h"
#include "../config_vars.h"
#include "../config.h"
}

#include "io_ramses.h"
#include "ramses/RAMSES_particle_data.hh"


#ifndef RAMSES_DMO
#include "ramses/RAMSES_amr_data.hh"
#include "ramses/RAMSES_hydro_data.hh"

typedef RAMSES::AMR::cell_locally_essential<> RAMSES_cell;
typedef RAMSES::AMR::tree< RAMSES_cell, RAMSES::AMR::level< RAMSES_cell > > RAMSES_tree;
typedef RAMSES::HYDRO::data< RAMSES_tree > RAMSES_hydro_data;
#endif

void str_replace( std::string &s, const std::string &search, const std::string &replace ) {
  for( size_t pos = 0; ; pos += replace.length() ) 
  {
    pos = s.find( search, pos );
    if( pos == std::string::npos ) break;

    s.erase( pos, search.length() );
    s.insert( pos, replace );
  }
}

void load_particles_ramses( const char* filename, struct particle **p, int64_t *num_p )
{
  int trimc;
  int64_t maxid, minid;
  maxid = 0;
  minid = 0;
  // Construnct info filenane
  // printf("%s\n", "Entered load_particles_ramses");
  std::stringstream ss;
  std::string info_filename;
  ss << filename;
  ss >> info_filename;

  std::string str_filename;
  ss.clear();
  ss << filename;
  ss >> str_filename;

  // LC: might have to modify this? Might be output number, not
  // processor number.
  // LC: had to modify this, it trims the processor number off the
  // file name
  if (RAMSES_SIX_DIGIT) {
    trimc = 6;
  } else {
    trimc = 5;
  }
  info_filename.resize( info_filename.size() - trimc );
  // std::cout << "str_filename = " << str_filename << "\n";
  // std::cout << "info_filename = " << info_filename << "\n";

  // Replace parts of part filename
  str_replace(info_filename, (std::string)"/part_", (std::string)"/info_");
  str_replace(info_filename, (std::string)".out", (std::string)".txt");
  //... open the snapshot info file

  // printf("%s\n", "Reading Info File...");

  RAMSES::snapshot rsnap( info_filename, RAMSES::version3 );

  // compute some units
  //const double kB      = 1.3806200e-16;
  //const double mH      = 1.6600000e-24;

  //double scale_v  = rsnap.m_header.unit_l / rsnap.m_header.unit_t;
  //double scale_T2 = mH / kB * scale_v * scale_v;
  double lfac = rsnap.m_header.unit_l / 3.08e24 * rsnap.m_header.H0 * 0.01 / rsnap.m_header.aexp;
  double mfac = rsnap.m_header.unit_d * pow(rsnap.m_header.unit_l,3)
    * 5.02739933e-34 * rsnap.m_header.H0 * 0.01;
  //double scale_m = mfac;

  //long long maxid_from_particles = 0;
      
#ifdef RAMSES_DMO
  // renormalize Omega_c in masses to Omega_m = Omega_c+Omega_b
  mfac *= rsnap.m_header.omega_m/(rsnap.m_header.omega_m-rsnap.m_header.omega_b);
  std::cout << "Compiled with -DRAMSES_DMO" << "\n";
#endif
      
  double vfac = rsnap.m_header.unit_l/rsnap.m_header.unit_t * 1e-5;
  //size_t total_count = 0;

  /* read data */
  double min_dm_mass = 1e30;
  size_t nump0 = *num_p;
  bool bdmonlyrun = false;


  // LC: 18/11/20 perhaps this is the bottleneck? if every snapshot
  // loops through all of the CPUs this is slow... e: yes this is the
  // bottleneck

      
  // printf("%s\n", "Determining minimum dark matter mass...");
  if (RAMSES_ZOOM) {

    // FIXME: check this works for zoom ramses runs in group
    // directories
    if (!RAMSES_GROUP_SIZE){
      fprintf(stderr, "[Error] <group> format not implemented for RAMSES zoom simulations.\n");
      exit(1);
    }

    // TODO: LC: 18/11/20 optimise this first branch heavily!!
    for( unsigned icpu=1; icpu<=rsnap.m_header.ncpu; ++icpu ) {
      // std::cout << "LC: working on CPU" << icpu << "\n";
      RAMSES::PART::data local_data( rsnap, icpu );
      try{
	std::vector<float> age, mass;
#ifdef LONGID
      std::vector<int64_t> ids;
#else
      std::vector<int> ids;
#endif
	// LC - read in 'tag' and 'family' fields, which are
	// hopefully integers 21/1
	std::vector<unsigned char> family, tag;

	local_data.get_var<double>("mass",std::back_inserter(mass));
	local_data.get_var<int>("particle_ID",std::back_inserter(ids));
	

	// LC - trying reading here 21/1
	try {
	  local_data.get_var<double>("age",std::back_inserter(age));
	} catch(...) {
	  bdmonlyrun = true;
	}
	  
	if( bdmonlyrun ) {
	  for( size_t i=0; i<mass.size(); ++i )
	    if( mass[i] < min_dm_mass )
	      min_dm_mass = mass[i];
      
	} else {
	  for( size_t i=0; i<mass.size(); ++i ) { 
	    if( RAMSES::PART::is_of_type( age[i], ids[i],
					  RAMSES::PART::ptype_dm)) {
	
		  min_dm_mass = mass[i];
	    }
	  }
	}
	// End of try region
      } catch(...) {
	//... then some read operation failed
	std::cerr << "something bad happened.\n";
	throw;
      }
      // End of CPU loop
    }
      
    } else {
    // LC: 18/11/20 otherwise we just need to read one CPU and one
    // particle mass
      
    // FIXME LC: 18/11/20 could there be a problem with a CPU not
    // having any particles? could add a check in
      
    // std::cout << "LC: working on CPU" << icpu << "\n";
    RAMSES::PART::data local_data( rsnap, 1 );
    try{
      std::vector<float> age, mass;
#ifdef LONGID
      std::vector<int64_t> ids;
#else
      std::vector<int> ids;
#endif
      // LC - read in 'tag' and 'family' fields, which are
      // hopefully integers 21/1
      std::vector<unsigned char> family, tag;
      
      local_data.get_var<double>("mass",std::back_inserter(mass));
      
#ifdef LONGID
      local_data.get_var<int64_t>("particle_ID",std::back_inserter(ids));
#else
      local_data.get_var<int>("particle_ID",std::back_inserter(ids));
#endif	

      // These are _all_ particle IDs, including stars
      for( size_t i=0; i<ids.size(); ++i ) {
#ifdef LONGID
	printf("1CPU ID: %" PRId64 "\n", ids[i]);
#else
	std::cout << "1CPU ID: " << ids[i] << "\n";
#endif
      }
      // LC - trying reading here 21/1
      try{
	local_data.get_var<double>("age",std::back_inserter(age));
      }
      catch(...){
	bdmonlyrun = true;
      }
	  
      if( bdmonlyrun ) {
	// LC: just read first DM particle as to get the mass
	min_dm_mass = mass[0];
      } else {
	// LC: otherwise, find the first DM particle and use this as
	// the mass
	for( size_t i=0; i<mass.size(); ++i ) {
	  if( RAMSES::PART::is_of_type( age[i], ids[i],
					RAMSES::PART::ptype_dm)){
	    
	    min_dm_mass = mass[i];
	    break;
	      }
	  // End of loop over particles
	}
      }
      // End of try region
    } catch(...) {
	//... then some read operation failed
	std::cerr << "something bad happened.\n";
	throw;
      }
    // End of if (RAMSES_ZOOM)
  }
    
  // printf("%s\n", "Done");
  double mminp = 1.01 * min_dm_mass;
  
  // Get CPU number from part filename
  // LC: modified
  // if (RAMSES_SIX_DIGIT) {
    // std::istringstream convert( str_filename.substr(str_filename.size() - 6) );
  // } else {
    // std::istringstream convert( str_filename.substr(str_filename.size() - 5) );
  // }
  std::istringstream convert( str_filename.substr(str_filename.size() - trimc) );
  unsigned icpu;

  if ( !(convert >> icpu) )
    icpu = 0;

  assert(icpu > 0);

  // printf("%s %d\n", "Working on CPU", icpu);

  // Read only this cpu
      RAMSES::PART::data local_data( rsnap, icpu );

      // printf("Reading RAMSES particle data for domain %d / %d...\n",icpu,rsnap.m_header.ncpu);
      std::vector<float> x, y, z, vx, vy, vz, age, mass;
#ifdef LONGID
      std::vector<int64_t> ids;
#else
      std::vector<int> ids;
#endif
      
      // LC - read in 'tag' and 'family' fields, which are
      // hopefully integers 21/1
      std::vector<signed char> family, tag;

      long np;
      np = 0;
      //        size_t local_particles = 0;
      
      
      try{
	local_data.get_var<double>("position_x",std::back_inserter(x));
        
	// we can preallocate memory for the rest
	y.reserve( x.size() );
	z.reserve( x.size() );
	vx.reserve( x.size() );
	vy.reserve( x.size() );
	vz.reserve( x.size() );
	ids.reserve( x.size() );
	age.reserve( x.size() );
	mass.reserve( x.size() );
	family.reserve( x.size() );
	tag.reserve( x.size() );

	local_data.get_var<double>("position_y",std::back_inserter(y));
	local_data.get_var<double>("position_z",std::back_inserter(z));
	local_data.get_var<double>("velocity_x",std::back_inserter(vx));
	local_data.get_var<double>("velocity_y",std::back_inserter(vy));
	local_data.get_var<double>("velocity_z",std::back_inserter(vz));
        
	//... and the particle ages
#ifdef LONGID
      local_data.get_var<int64_t>("particle_ID",std::back_inserter(ids));
#else
      local_data.get_var<int>("particle_ID",std::back_inserter(ids));
#endif	

	local_data.get_var<double>("mass",std::back_inserter(mass));
        
	// LC - read in the extra fields 21/1
	if (RAMSES_EXTRA_FIELDS == 1){
	  local_data.get_var<signed char>("family",std::back_inserter(family));
	  local_data.get_var<signed char>("tag",std::back_inserter(tag));
	}

	if( !bdmonlyrun )
	  local_data.get_var<double>("age",std::back_inserter(age));
	

	size_t local_particles = 0;
	if( bdmonlyrun ){
	  for( size_t i=0; i<x.size(); ++i )
	    if( mass[i] < mminp )
	      local_particles++;
	} else if (!RAMSES_ZOOM && RAMSES_EXTRA_FIELDS) {
	  // LC: if we aren't doing a zoom, and we have the extra fields,
	  // we know that DM particles will have a value of 1 for
	  // family, go ahead and use this
	  for( size_t i=0; i<x.size(); ++i ){
	    if (static_cast<int>(family[i]) == 1) {
	      local_particles++;
	    	    // TESTING
// 	      std::cout << "LC TESTING:" << static_cast<signed>(family[i]) << " " << mass[i] << "\n";
// #ifdef LONGID
// 	      printf("ID: %" PRId64 "\n", ids[i]);
// #else
// 	      std::cout << "ID: " << ids[i] << "\n";
// #endif
	    }
	  }
	} else {
	  for( size_t i=0; i<x.size(); ++i ){
	    if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) && mass[i] < mminp )
	      local_particles++;
	  }
	}
	
	*p = (struct particle *)check_realloc(*p, ((*num_p)+local_particles)*sizeof(struct particle), "Allocating particles.");

	size_t j=0;
	if (!RAMSES_ZOOM && RAMSES_EXTRA_FIELDS) {
	  // LC: if we aren't doing a zoom, and we have the extra fields,
	  // we know that DM particles will have a value of 1 for
	  // family, go ahead and use this
	  for( size_t i=0; i<x.size(); ++i ) {
	    if (static_cast<int>(family[i]) == 1) {
	      (*p)[*num_p+j].pos[0] = fmod(x[i]+1.0,1.0) * lfac;
	      (*p)[*num_p+j].pos[1] = fmod(y[i]+1.0,1.0) * lfac;
	      (*p)[*num_p+j].pos[2] = fmod(z[i]+1.0,1.0) * lfac;
	      (*p)[*num_p+j].pos[3] = vx[i] * vfac;
	      (*p)[*num_p+j].pos[4] = vy[i] * vfac;
	      (*p)[*num_p+j].pos[5] = vz[i] * vfac;
	      (*p)[*num_p+j].id     = ids[i];
	      if (ids[i] > maxid)
		maxid = ids[i];
	      if (ids[i] < minid)
		minid = ids[i];

	      ++j;
	    } // end of if
	  } // end of for
	} else {
	  for( size_t i=0; i<x.size(); ++i ) {
	    if( mass[i] < mminp && 
		( bdmonlyrun || ( !bdmonlyrun && RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) ) ) ) {
		(*p)[*num_p+j].pos[0] = fmod(x[i]+1.0,1.0) * lfac;
		(*p)[*num_p+j].pos[1] = fmod(y[i]+1.0,1.0) * lfac;
		(*p)[*num_p+j].pos[2] = fmod(z[i]+1.0,1.0) * lfac;
		(*p)[*num_p+j].pos[3] = vx[i] * vfac;
		(*p)[*num_p+j].pos[4] = vy[i] * vfac;
		(*p)[*num_p+j].pos[5] = vz[i] * vfac;
		(*p)[*num_p+j].id     = ids[i];
		if (ids[i] > maxid)
		  maxid = ids[i];
		if (ids[i] < minid)
		  minid = ids[i];

		++j;
	    } // end of if
	  } // end of for
	}  // end of if-else
	assert( j==local_particles);
	*num_p += local_particles;
	// LC testing
	np = local_particles;
        
      }catch(...)
        {
	  //... then some read operation failed
	  std::cerr << "something bad happened.\n";
	  throw;
        }

  
  /* set the code parameters */
  Ol = rsnap.m_header.omega_l;
  Om = rsnap.m_header.omega_m;
  h0 = rsnap.m_header.H0 * 0.01;
  BOX_SIZE = rsnap.m_header.unit_l  / 3.08e24 * rsnap.m_header.H0 * 0.01 / rsnap.m_header.aexp;
  //BOX_SIZE = rsnap.m_header.unit_l  / 3.08567758096e24 * rsnap.m_header.H0 * 0.01 / rsnap.m_header.aexp;
  SCALE_NOW = rsnap.m_header.aexp;
  PARTICLE_MASS = min_dm_mass * mfac;
  TOTAL_PARTICLES = *num_p - nump0;

  if (!RAMSES_ZOOM) {
    // If unigrid, then 2**3levelmin gives the total number of
    // particles
    TOTAL_PARTICLES = 1L << (3*rsnap.m_header.levelmin);

    // Since we only work on dark matter, rescale the dark matter
    // particle mass as if it were in a DMO run
    if (RESCALE_PARTICLE_MASS) {
      // std::cout << "Rescaling particle mass" << "\n"
      PARTICLE_MASS = Om*CRITICAL_DENSITY * pow(BOX_SIZE, 3) / TOTAL_PARTICLES;
    } // end of if (RESCALE_PARTICLE_MASS)
    
  } // end of if (!RAMSES_ZOOM) 

  printf("read %ld particles and the maxid: %ld  minid: %ld \n", np, maxid, minid);
  
  
  AVG_PARTICLE_SPACING = cbrt(PARTICLE_MASS / (Om*CRITICAL_DENSITY));
  // printf("Read %lld particles from RAMSES unigrid snapshot, boxsize = %f, PARTICLE_MASS = %g, TOTAL_PARTICLES = %lld, SCALE_NOW = %f, AVG_PARTICLE_SPACING = %f\n", *num_p - nump0, BOX_SIZE, PARTICLE_MASS, TOTAL_PARTICLES, SCALE_NOW, AVG_PARTICLE_SPACING );


    
}
