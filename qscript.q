#!/bin/sh
#$ -N run_rockstar
#$ -m bea
#$ -cwd
#$ -pe openmpi 256
###$ -q mps.q@@compute_amd_c6145_mps
#$ -q mps.q
#$ -S /bin/bash
# source modules environment:
module add gcc/4.8.1
module add openmpi/gcc/64/1.7.3

set -x

exe=/home/d/ds/ds381/rockstar-ramses/rockstar

echo Entering $(pwd)

rm auto-rockstar.cfg test-output.dat
$exe -c ramses.cfg >& server.dat &
perl -e 'sleep 1 while (!(-e "auto-rockstar.cfg"))'

mpirun -np $NSLOTS $exe -c auto-rockstar.cfg
